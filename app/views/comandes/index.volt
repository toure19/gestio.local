<html>
<head>


    <section class="content container-fluid">


        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Tabla de comandes</h3>

                    <div class="box-tools">
                        <div class="input-group input-group-sm" style="width: 150px;">
                            <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">

                            <div class="input-group-btn">
                                <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                        <tbody>
                        <tr>
                            <th>Num.</th>
                            <th>Data</th>
                            <th>Usuari</th>
                            <th>Total</th>
                            <th>Servida</th>
                            <th>Observacions</th>
                        </tr>
                        {% set total=0 %}
                        {% for comanda in arrayComandes %}

                            <tr>
                                <td>{{ comanda.numero }}</td>
                                <td>{{ comanda.data }}</td>
                                <td>{{ comanda.usuari }}</td>
                                <td>{{ comanda.total }}€</td>
                                <td class="badge {% if comanda.servida == "si" %}bg-green{% else %}bg-yellow{% endif %}">{{ comanda.servida|capitalize }}</td>
                                <td>{{ comanda.observacions }}</td>
                                <td style="margin: 0px;padding: 2px;">
                                    <a href="{{ url('comandes/comanda/'~comanda.numero) }}" type="button"
                                       class="btn btn-info"><i class="fa fa-edit"></i></a>
                                </td>
                            </tr>
                            {% set total = total + comanda.total %}
                        {% endfor %}

                        </tbody>
                    </table>
                    <div style="margin: 10px;">
                        <hr>
                        <p>Num. comandes: <label class="badge bg-green"> {{ arrayComandes|length }} </label>
                        <hr>
                        Total : <label class="badge bg-aqua-active color-palette"> {{ total }} € </label></p>
                        <hr>
                        <a href="comandes/novaComanda" type="button" class="btn btn-success"><i class="fa fa-plus"> Nova comanda</i></a>
                        {#{% if session.get("tipus")=='U' %}
                        disabled {% endif %}>#}
                    </div>
                </div>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
        </div>


    </section>


    <!-- REQUIRED JS SCRIPTS -->

    <!-- jQuery 3 -->
    <script src="public/adminlte/bower_components/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap 3.3.7 -->
    <script src="public/adminlte/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- AdminLTE App -->
    <script src="public/adminlte/dist/js/adminlte.min.js"></script>


    <!-- Optionally, you can add Slimscroll and FastClick plugins.
         Both of these plugins are recommended to enhance the
         user experience. -->
    <script>
        //resaltar l'opció de menu activa:
        $(document).ready(function () {
            var cambio = false;
            $('.sidebar-menu li a').each(function (index) {
                if (this.href.trim() == window.location) {
                    $(this).parent().addClass("active");
                    cambio = true;
                } else {
                    $(this).parent().removeClass("active");
                    cambio = false;
                }
            });
            if (!cambio) {
                $('.sidebar-menu li:first').addClass("active");
            }

        });
    </script>
    <script type="text/javascript" src="public/js/bootstrap-filestyle.min.js"></script>
