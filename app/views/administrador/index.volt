<section class="content container-fluid">

    <div class="row">
        <div class="col-lg-4 col-xs-8">
            <!-- small box -->
            <div class="small-box bg-aqua">
                <div class="inner">
                    <h3>45</h3>

                    <p>Pedidos Nuevos</p>
                </div>
                <div class="icon">
                    <i class="fa fa-shopping-cart"></i>
                </div>
                <a href="#" class="small-box-footer">
                    Clientes <i class="fa fa-arrow-circle-right"></i>
                </a>
            </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-4 col-xs-8">
            <!-- small box -->
            <div class="small-box bg-red">
                <div class="inner">
                    <h3>55<sup style="font-size: 20px">%</sup></h3>

                    <p>Morosos</p>
                </div>
                <div class="icon">
                    <i class="ion ion-stats-bars"></i>
                </div>
                <a href="#" class="small-box-footer">
                    Contabilidad <i class="fa fa-arrow-circle-right"></i>
                </a>
            </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-4 col-xs-8">
            <!-- small box -->
            <div class="small-box bg-green">
                <div class="inner">
                    <h3>10</h3>

                    <p>Nuevos Clientes</p>
                </div>
                <div class="icon">
                    <i class="ion ion-person-add"></i>
                </div>
                <a href="#" class="small-box-footer">
                    Pedidos <i class="fa fa-arrow-circle-right"></i>
                </a>
            </div>
        </div>
        <!-- ./col -->

    </div>
    <!-- ./col -->
</section>