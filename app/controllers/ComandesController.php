<?php
/**
 * Created by PhpStorm.
 * User: toure19_notebook
 * Date: 24/01/2019
 * Time: 19:16
 */

class ComandesController extends ControllerBase
{
// s’executa sempre abans que una action...a l’igual que initialize, però beforeExecuteRoute pot parar l’execució
    public function beforeExecuteRoute($dispatcher)
    {
        // controlem login OK
        if (!$this->session->has('clau')) {
            $this->dispatcher->forward(array(
                "controller" => "index",
                "action" => "index"));
            return false; //parem l'execució del controller per a que torne a fer login
        }
        if ($this->session->tipus === "U") {
            $this->view->setTemplateBefore("plantilla_usuari");
        } else {
            $this->view->setTemplateBefore("plantilla_administrador");
        }
    }

    public function indexAction()
    {
        $comandes = Comandes::find('usuari' === $this->session->get('clau'));
        $this->view->setVar("arrayComandes", $comandes);
    }

    public function comandaAction($numComanda)
    {

        $comanda = Comandes::findFirst($numComanda);
        $this->view->setVar("comanda", $comanda);
    }

    public function novaComandaAction()
    {
    }
}