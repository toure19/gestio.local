<?php

class PagamentsController extends ControllerBase
{


    // s’executa sempre abans que una action...a l’igual que initialize, però beforeExecuteRoute pot parar l’execució
    public function beforeExecuteRoute($dispatcher)
    {
        // controlem login OK
        if (!$this->session->has('clau')) {
            $this->dispatcher->forward(array(
                "controller" => "index",
                "action" => "index"));
            return false; //parem l'execució del controller per a que torne a fer login
        }
        if ($this->session->tipus === "U") {
            $this->view->setTemplateBefore("plantilla_usuari");
        } else {
            $this->view->setTemplateBefore("plantilla_administrador");
        }
    }

    public function indexAction()
    {
        $pagaments = Pagaments::find('usuari' === $this->session->get('clau'));
        $this->view->setVar("arrayPagaments", $pagaments);
    }

    public function pagamentAction($numPagament)
    {

        $pagament = Pagaments::findFirst($numPagament);
        $this->view->setVar("pagament", $pagament);
    }

}

