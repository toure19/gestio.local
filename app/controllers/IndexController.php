<?php

class IndexController extends ControllerBase
{

    public function indexAction()
    {
        if (!$this->session->has('clau')) {
            // Redireccionem al login. L’usuari no ha iniciat sessió
            $this->dispatcher->forward(array(
                'action' => 'login'
            ));
        } else { //sessió iniciada, redirigim segons usuari
            if ($this->session->tipus == "U") { //és un usuari normal
                $this->dispatcher->forward(array(
                    'controller' => 'usuari'));
            } elseif ($this->session->tipus == "A") { //és un usuari admin
                $this->dispatcher->forward(array(
                    'controller' => 'administrador'));
            }
        }
    }

    public function loginAction()
    {

    }

    public function comprovaLoginAction()
    {
        if ($this->request->isPost()) { //comprovem si les dades arriben pel formulari
            $pass = $this->request->getPost("password");
            $email = $this->request->getPost("email");
        } else {//no es post?
            $this->view->setVar("errores", "Error en les dades");
            $this->dispatcher->forward(array('action' => 'login'));
            return false;
        }
        //cal trobar el tipus d'usuari si està registrat
        $usuari = Usuaris::findFirstByEmail($email);
        if ($usuari) { //està registrat
            if ($this->security->checkHash($pass, $usuari->contra)) { // comprovem contrasenya encriptà
                $this->session->clau = $usuari->getId(); // assignem a la var de sessió l’ id
                if ($usuari->getTipus() == 'U') {
                    $this->session->tipus = 'U'; // assignem la var de sessió tipus
                } elseif ($usuari->getTipus() == 'A') {
                    $this->session->tipus = 'A'; // assignem la var de sessió tipus
                } else {
                    $this->view->setVar("error", "Tipus d'Usuari No definit!");
                }
            } else { //contrasenya incorrecta
                $this->view->setVar("error", "Usuari/password incorrecte");
                $this->security->hash(rand()); //canviem hash per a fer més difícil la fuerza bruta...
            }
        } else {//no està registrat
            $this->view->setVar("error", "Usuari/password incorrecte");
        }
        //enviem a index per a que gestione segons el tipus
        $this->dispatcher->forward(array(
            'action' => 'index'));

    }

    public function logoutAction()
    {
        //borrem variables de sessió
        $this->session->remove('clau');
        $this->session->remove('tipus');
        //tanquem la sessió
        $this->session->destroy();
        //redirigim al login
        $this->dispatcher->forward(array(
            'controller' => 'index',
            'action' => 'index'
        ));
    }

    public function contraAction($id, $contra) //$id -> ide de l’usuaria canviar pass
    { //$pass -> contrasenya a encriptar
        $us = Usuaris::findFirstById($id);
        $us->setContra($this->security->hash($contra));
        $us->save();
    }

}

